const RESULT = {
  resolve: 'Promise fulfilled',
  reject: 'reject rejected',
};

const callPromise = () => new Promise((resolve, reject) => {
  const success = (Math.floor(Math.random() * 200) + 1 ) > 100;

  setTimeout(() => {
    if (success) {
      resolve(RESULT.resolve);
    } else {
      reject(new Error(RESULT.reject));
    }
  }, 900);
})

function task1(){
  callPromise().then(response => {
    console.log(response)
  }).catch(err => {
    alert('Произошла ошибка');
  });

}

async function task2(){
  try{
    let response = await callPromise()
    console.log(response);
  } catch(err){
    alert('Произошла ошибка')
  }
}

task1()
task2()