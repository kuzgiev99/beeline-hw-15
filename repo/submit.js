let form = document.querySelector('#myForm');

function sendForm(message) {
    let div = document.createElement('div');
    div.innerText = message;

    div.style.cssText=`
    background: #dd567e;
    width: 200px;
    float: right ;
    padding: 10px;
    text-align: center;
    font-size: 16px;
    line-height: 22px;
    font-weight: 700;
    color: #f9f9f9;
    border-radius: 10px;
    `;
  form.append(div);
}

function createInput(name, value) {
    let inputForm = document.createElement('input');
    inputForm.type = "hidden";
    inputForm.setAttribute("name", name);
    inputForm.setAttribute("value", value);

    return inputForm;
}

function main(){
    let deleteInput = document.querySelector('input[name="hiddenInput"]');

    deleteInput.remove();

    let conteiner = form.querySelectorAll('.field-container');

    conteiner[6].append(createInput("newField", "customValue"))
    sendForm("Форма отправлена!")
}

form.addEventListener('submit', main );


